# Video-Decoder/Encoder (ffmpeg custom-build) 
---
##### customized ffmpeg build --- `(Alexander Weiß 05.02.2016)`
---
## Übersicht
Zielplattform: Ubuntu 14.04 amd64 
#### Umfang                                                        
* x265 HEVC Encoder                                             
* x264_0.148.2+gita01e339-1_amd64.deb                           
* libvpx-1.5.0_0-1_amd64.deb                                    
* fdk-aac_0.1.0-1_amd64.deb                                     
* qt-faststart_201602051242-git-1_amd64.deb                     
* ffmpeg_201602051221-git-1_amd64.deb                           
>  * ffprobe                                                   
>  * ffplay                                                    
>  * ffserver                                                  
>  * vpxdec / vpxdec                                           

## Installationsvarianten 

```
sudo su
if [ ! -d /opt/pc4 ] ; then mkdir /opt/pc4 ; fi 
cd /opt/pc4
git clone git@bitbucket.org:albus-fh/ffmpeg-custom-build.git 

```

### 1. automatische Installation mit Fehlererkennung

1. "Versuch" (2.): Installation von deb-Paket 
2. "Versuch" (3.): Build aus aktuellestem Quellcode 
3. "Versuch" (4.): ReadyToUseBuild incl. Bibliotheken (ubuntu 14.04 amd64)
                  (kompiliert und gelinkt)  

```
. /opt/pc4/ffmpeg-custom-build/install/ffmpeg_inst.sh 

```
Die folgenden drei Möglichkeiten lassen sich natürlich auch seperat ausführen:

### 2. Installation aus prekompiliertem deb-Paketen 
```
. /opt/pc4/ffmpeg-custom-build/install/install-ffmpeg.sh

```
### 3.Neues Build aus aktuellem Quellcode   
```
. /opt/pc4/ffmpeg-custom-build/install/build-ffmpeg.sh

```
### 4. ReadyToUseBuild (ubuntu-14.04 amd64)
Auf Ubuntu-14.04 amd64 Server kompiliertes und gelinktes Build incl.wichtiger Bibliotheken.     
```
cd /opt/pc4/ffmpeg-custom-build
ln /opt/pc4/ffmpeg-custom-build/portable/* /usr/local/bin 
$PATH=/usr/local/bin:$PATH

```
---

## Verwendung ffmpeg
---