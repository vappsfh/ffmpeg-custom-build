#!/bin/bash                                                               #
## -----------------------------------------------------------------------#
## Setup DreamFactory / ffmpeg / postgreSQL                               #
# ------------------------------------------------------------------------# 
# Author: Alexander Weiß                                                  # 
# 05.01.2016                                                              #
# ------------------------------------------------------------------------# 
# Copyright (c) 2016, Alexander Weiß (FH-Bingen Projektteam VAPPS)        # 
#                                                                         # 
# Permission to use,copy, modify, and/or distribute this software for any # 
# purpose with or without fee is hereby granted, provided that the above  # 
# copyright notice and this permission notice appear in all copies.       # 
#                                                                         #
# THE SOFTWARE IS PROVIDED AS IS AND THE AUTHOR DISCLAIMS ALL WARRANTIES  # 
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF        # 
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR # 
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES  # 
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN   # 
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF # 
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.          # 
# ######################################################################### 
## highlightning with some colors
## highlightning with some colors
function logError()  { cat /dev/stdin | sed 's,.*,\x1B[31m&\x1B[0m,' >&1; }      #red
function highlight() { cat /dev/stdin | sed 's,.*,\x1B[33m&\x1B[0m,' >&1; }      #yello
function myecho()    { cat /dev/stdin | sed 's,.*,\x1B[32m&\x1B[0m,' >&1; } 

export -f logError
export -f highlight
export -f myecho
highlight<<YELLO
## ########################################################################
## ffmpeg - Video-Decoder/Encoder                                         #
## ########################################################################
##                                                                        #
## Installation von ffmpeg/ffprob/ffplay/ffserver + aller libs            #
## von vorab kompilierten DEB-Packeten (incl. x264-codec).                #
##                                                                        #
## -----------------------------------------------------------------------#
##  Inhalt:                                                               #
##  - x265 HEVC Encoder                                                   #
##  - x264_0.148.2+gita01e339-1_amd64.deb                                 #
##  - libvpx-1.5.0_0-1_amd64.deb                                          #
##  - fdk-aac_0.1.0-1_amd64.deb                                           #
##  - qt-faststart_201602051242-git-1_amd64.deb                           #
##  - ffmpeg_201602051221-git-1_amd64.deb                                 #
##      + ffprobe                                                         #
##      + ffplay                                                          #
##      + ffserver                                                        #
##      + vpxdec / vpxdec                                                 #
## -----------------------------------------------------------------------#
## Kompiliert am 05.02.2016                                               #
## Alexander Weiß                                                         #
## -----------------------------------------------------------------------# 
YELLO

exec 3> logError

myecho<<green
###########################################################################
## starte Installation aus precompilierten DPKG 
###########################################################################
green

. $FF_MPEG_DIR/install/install-ffmpeg.sh  > >(tee ~/ffmpeg-install.log) | myecho \ 
		              2> >(tee ~/ffmpeg-install.error >&3)

ffmpeg -h >/dev/null; 
					
if [ $? -gt 0 ]
then 
  echo "Installation fehlgeschlagen! 
  Installationvariante 2 wird gestartet." | tee -a ~/ffmpeg-install.log | myecho
	
  . $FF_MPEG_DIR/install/build-ffmpeg.sh  > >(tee ~/ffmpeg-build.log) 
                      2> >(tee ~/ffmpeg-build.error >&3)
fi

ffmpeg -h >/dev/null;
if [ $? -gt 0 ]
then 
  echo "Erstellen eines neuen ffmpeg-builds fehlgeschlagen.   
       Starte readyToUseBuild Installation" | tee -a ~/ffmpeg-build.log | myecho  				  

 . $FF_MPEG_DIR/install/readyToUse.sh >(tee ~/ffmpeg-build.log) | myecho \
                      2> >(tee ~/ffmpeg-build.error >&3)

fi

ffmpeg -h >/dev/null;
if [ $? -gt 0 ] 
  then 
  echo "Kritischer Fehler: Installation von ffmpeg endgültig nicht 
        erfolgreich. Bitte versuchen Sie eine manuelle Installation" >logError

  echo "logs-files: /root/..."  > logError
fi 

&3-
## ENDE ffmpeg
## ------------------------------------------------------------------------


