#!/bin/sh                                                                 # 
# Installiert das ReadyToUseBuild von ffmpeg.                             #
# ffmpeg incl. der meisten libs (libx264 / x265)                          #
# ------------------------------------------------------------------------# 
# Author: Alexander Weiß                                                  # 
# 05.02.2016                                                              #
# ------------------------------------------------------------------------# 
# Copyright (c) 2016, Alexander Weiß (FH-Bingen Projektteam VAPPS)        # 
#                                                                         # 
# Permission to use,copy, modify, and/or distribute this software for any # 
# purpose with or without fee is hereby granted, provided that the above  # 
# copyright notice and this permission notice appear in all copies.       # 
#                                                                         #
# THE SOFTWARE IS PROVIDED AS IS AND THE AUTHOR DISCLAIMS ALL WARRANTIES  # 
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF        # 
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR # 
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES  # 
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN   # 
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF # 
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.          # 
# ######################################################################### 

## highlightning with some colors
function logError()  { cat /dev/stdin | sed 's,.*,\x1B[31m&\x1B[0m,' >&1; }      #red
function highlight() { cat /dev/stdin | sed 's,.*,\x1B[33m&\x1B[0m,' >&1; }      #yello
function myecho()    { cat /dev/stdin | sed 's,.*,\x1B[32m&\x1B[0m,' >&1; } 


## libs
apt-get -y update
apt-get -y dist-upgrade 
apt-get -y install dpkg 
apt-get -y install build-essential checkinstall git libfaac-dev libgpac-dev 
apt-get -y install libjack-jackd2-dev libopencore-amrnb-dev libopencore-amrwb-dev 
apt-get -y install librtmp-dev libsdl1.2-dev libtheora-dev libva-dev libvdpau-dev 
apt-get -y install libx11-dev libxfixes-dev pkg-config texi2html yasm zlib1g-dev 
apt-get -y install pkg-config libxcb1-dev libxcb-shm0-dev libxcb-xfixes0-dev  
apt-get -y install librtmp-dev yasm libfreetype6-dev libass-dev libtool autoconf 
apt-get -y install texinfo libopus-dev libvorbis-dev libmp3lame-dev automake 

for link in $(ls $FF_MPEG_DIR/portable) ; do rm /usr/local/bin/$link 2>/dev/null; done
rm -Rf $FF_MPEG_DIR/portable 2>/dev/null


## clone git 
if [ ! -d $PC_INST_DIR ]  
	then 
	mkdir /opt/pc4
	export PC_INST_DIR=/opt/pc4
	echo PC_INST_DIR=/opt/pc4 >> environment
cd $PC_INSTALL_DIR 
git clone git@bitbucket.org:albus-fh/ffmpeg-custom-build.git $FF_MPEG_DIR
fi

cd $FF_MPEG_DIR
tar -xJf portable.tar.xz || tar xjf portable.tar.bz || tar xzf portable.tar.gz
## publish apps
ln $FF_MPEG_DIR/portable/* /usr/local/bin 
echo $PATH | grep /usr/local/bin
if [ $? -gt 0 ]; then 
$PATH=/usr/local/bin:$PATH
fi

ffmpeg -h >/dev/null;
if [ $? -gt 0 ]
then 
echo "ERROR: ffmpeg sollte nun funktionieren" ;
fi