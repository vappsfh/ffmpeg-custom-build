#!/bin/sh                                                                 #
## highlightning with some colors
function logError()  { cat /dev/stdin | sed 's,.*,\x1B[31m&\x1B[0m,' >&1; }      #red
function highlight() { cat /dev/stdin | sed 's,.*,\x1B[33m&\x1B[0m,' >&1; }      #yello
function myecho()    { cat /dev/stdin | sed 's,.*,\x1B[32m&\x1B[0m,' >&1; } 
# Installationsscript für ffmpeg als Placity-Backend-Module               #
# ------------------------------------------------------------------------# 
# Author: Alexander Weiß                                                  # 
# 05.01.2016                                                              #
# ------------------------------------------------------------------------# 
# Copyright (c) 2016, Alexander Weiß (FH-Bingen Projektteam VAPPS)        # 
#                                                                         # 
# Permission to use,copy, modify, and/or distribute this software for any # 
# purpose with or without fee is hereby granted, provided that the above  # 
# copyright notice and this permission notice appear in all copies.       # 
#                                                                         #
# THE SOFTWARE IS PROVIDED AS IS AND THE AUTHOR DISCLAIMS ALL WARRANTIES  # 
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF        # 
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR # 
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES  # 
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN   # 
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF # 
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.          # 
# #########################################################################  
highlight<<yello
## prepare ubuntu
yello
apt-get -y update
apt-get -y dist-upgrade 
apt-get -y install git dpkg 
apt-get -y install build-essential checkinstall libfaac-dev libgpac-dev 
apt-get -y install libjack-jackd2-dev libopencore-amrnb-dev libopencore-amrwb-dev 
apt-get -y install librtmp-dev libsdl1.2-dev libtheora-dev libva-dev libvdpau-dev 
apt-get -y install libx11-dev libxfixes-dev pkg-config texi2html yasm zlib1g-dev 
apt-get -y install pkg-config libxcb1-dev libxcb-shm0-dev libxcb-xfixes0-dev  
apt-get -y install librtmp-dev yasm libfreetype6-dev libass-dev libtool autoconf 
apt-get -y install texinfo libopus-dev libvorbis-dev libmp3lame-dev automake 
 
apt-get remove ffmpeg x265 x264 libav-tools libvpx-dev libx264-dev qt-faststart 

highlight<<yello
## ########################################################################
## ffmpeg - Video-Decoder/Encoder                                         #
## ########################################################################
##                                                                        #
## Installation von ffmpeg/ffprob/ffplay/ffserver + aller libs            #
## von vorab kompilierten DEB-Packeten (incl. x264-codec).                #
##                                                                        #
## -----------------------------------------------------------------------#
##  Inhalt:                                                               #
##  - x265 HEVC Encoder                                                   #
##  - x264_0.148.2+gita01e339-1_amd64.deb                                 #
##  - libvpx-1.5.0_0-1_amd64.deb                                          #
##  - fdk-aac_0.1.0-1_amd64.deb                                           #
##  - qt-faststart_201602051242-git-1_amd64.deb                           #
##  - ffmpeg_201602051221-git-1_amd64.deb                                 #
##      + ffprobe                                                         #
##      + ffplay                                                          #
##      + ffserver                                                        #
##      + vpxdec / vpxdec                                                 #
## -----------------------------------------------------------------------#
## Kompiliert am 05.02.2016                                               #
## Alexander Weiß                                                         #
## -----------------------------------------------------------------------# 
yello
## clone ffmpeg-prepare ubuntu from git  
if [ ! -d $PC_INST_DIR ]  
	then 
	mkdir /opt/pc4
	export PC_INST_DIR=/opt/pc4
	echo PC_INST_DIR=/opt/pc4 >> environment
cd $PC_INST_DIR
git clone git@bitbucket.org:albus-fh/ffmpeg-custom-build.git $FF_MPEG_DIR
fi


## install deb-packages
dpkg -i $FF_MPEG_DIR/deb/*.deb 
apt-get -y update
apt-get -y dist-upgrade 
apt-get -y -f install 


##ENDE ffmpeg 
########################################################################### 


##apt-get -y remove autoconf automake ffmpeg x264 x265 build-essential \ 
##           libass-dev libfreetype6-dev libsdl1.2-dev libsdl1.2-dev   \
##           libtheora-dev libtool libva-dev libvdpau-dev pkg-config   \
##           libxcb1-dev libxcb-shm0-dev libxcb-xfixes0-dev            \
##           texinfo zlib1g-dev checkinstall libopencore-amrnb-dev     \
##           libfaac-dev libgpac-dev libmp3lame-dev librtmp-dev yasm   \
##           libopencore-amrwb-dev texi2html libvorbis-dev             \
##		     libmp3lame-dev   
	 

  
