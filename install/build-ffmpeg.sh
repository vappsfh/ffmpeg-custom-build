#!/bin/bash
## highlightning with some colors
function logError()  { cat /dev/stdin | sed 's,.*,\x1B[31m&\x1B[0m,' >&1; }      #red
function highlight() { cat /dev/stdin | sed 's,.*,\x1B[33m&\x1B[0m,' >&1; }      #yello
function myecho()    { cat /dev/stdin | sed 's,.*,\x1B[32m&\x1B[0m,' >&1; } 
    
highlight<<yello                                                      
# Compile FFmpeg from source code - Installationsvariante 2               #
# ------------------------------------------------------------------------# 
# Author: Alexander Weiß                                                  # 
# 05.02.2016                                                              #
# ------------------------------------------------------------------------# 
# Copyright (c) 2016, Alexander Weiß (FH-Bingen Projektteam VAPPS)        # 
#                                                                         # 
# Permission to use,copy, modify, and/or distribute this software for any # 
# purpose with or without fee is hereby granted, provided that the above  # 
# copyright notice and this permission notice appear in all copies.       # 
#                                                                         #
# THE SOFTWARE IS PROVIDED AS IS AND THE AUTHOR DISCLAIMS ALL WARRANTIES  # 
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF        # 
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR # 
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES  # 
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN   # 
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF # 
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.          # 
# ######################################################################### 
## prepare ubuntu
yello

highlight<<yello
## Preparation
yello

apt-get -y update
apt-get -y dist-upgrade 


highlight<<yello
## Remove existing packages
yello
    
apt-get remove ffmpeg x264 libav-tools libvpx-dev libx264-dev
highlight<<yello
## dependencies
yello

apt-get update
apt-get -y install git dpkg
apt-get -y install mercurial
apt-get -y install cmake
apt-get -y install yasm
apt-get -y install cmake
apt-get -y install build-essential checkinstall git libfaac-dev libgpac-dev   \
libjack-jackd2-dev libmp3lame-dev libopencore-amrnb-dev libopencore-amrwb-dev \
librtmp-dev libsdl1.2-dev libtheora-dev libva-dev libvdpau-dev libvorbis-dev  \
libx11-dev libxfixes-dev pkg-config texi2html yasm zlib1g-dev


apt-get -y install libmp3lame-dev
apt-get -y install libopus-dev

instFromSource(){ 
cd                      
wget $1                       || $(echo "FEHLER: BUILDING $2 -wget"          | tee -a instFromSource.err | logError && return) 
if [ ! -f $2 ]
tar xfJ $2* 
tar xzj $2*
tar xzf $2*                  
cd $2*                        || $(echo "FEHLER: BUILDING $2 -cd $2*"        | tee -a instFromSource.err | logError && return)
./configure                   || $(echo "FEHLER: BUILDING $2 -./configure "  | tee -a instFromSource.err | logError && return)
make                          || $(echo "FEHLER: BUILDING $2 -make"          | tee -a instFromSource.err | logError && return)
checkinstall -y --pkgname=$2  || $(echo "FEHLER: BUILDING $2 -checkinstall"  | tee -a instFromSource.err | logError && return) 
apt-get install -f -y         || $(echo "FEHLER: BUILDING $2 -apt-get"       | tee -a instFromSource.err | logError && return)
};

export instFromSource
instFromSource http://johnvansickle.com/ffmpeg/git-source/freetype-2.6.1.tar.xz  freetype         ; if [ $? -gt 0 ] ; then echo FEHLER:freetype     >> tee -a instFromSource.err ; else echo OK:freetype     >> tee -a instFromSource.err ; fi      
instFromSource http://johnvansickle.com/ffmpeg/git-source/frei0r-plugins-1.4.tar.xz frei0r        ; if [ $? -gt 0 ] ; then echo FEHLER:frei0r       >> tee -a instFromSource.err ; else echo OK:frei0r       >> tee -a instFromSource.err ; fi   
instFromSource http://johnvansickle.com/ffmpeg/git-source/lame-3.99.5.tar.xz  lame                ; if [ $? -gt 0 ] ; then echo FEHLER:lame         >> tee -a instFromSource.err ; else echo OK:lame         >> tee -a instFromSource.err ; fi         
instFromSource http://johnvansickle.com/ffmpeg/git-source/libass-git.tar.xz   libass              ; if [ $? -gt 0 ] ; then echo FEHLER:libass       >> tee -a instFromSource.err ; else echo OK:libass       >> tee -a instFromSource.err ; fi         
instFromSource http://johnvansickle.com/ffmpeg/git-source/libsoxr-git.tar.xz   libsoxr            ; if [ $? -gt 0 ] ; then echo FEHLER:libsoxr      >> tee -a instFromSource.err ; else echo OK:libsoxr      >> tee -a instFromSource.err ; fi         
instFromSource http://johnvansickle.com/ffmpeg/git-source/libvidstab-git.tar.xz   libvidstab      ; if [ $? -gt 0 ] ; then echo FEHLER:libvidstab   >> tee -a instFromSource.err ; else echo OK:libvidstab   >> tee -a instFromSource.err ; fi     
instFromSource http://johnvansickle.com/ffmpeg/git-source/libvorbis-1.3.4.tar.xz libvorbis        ; if [ $? -gt 0 ] ; then echo FEHLER:libvorbis    >> tee -a instFromSource.err ; else echo OK:libvorbis    >> tee -a instFromSource.err ; fi      
instFromSource http://johnvansickle.com/ffmpeg/git-source/libvpx-git.tar.xz libvpx-git libvpxs    ; if [ $? -gt 0 ] ; then echo FEHLER:libvpxs      >> tee -a instFromSource.err ; else echo OK:libvpxs      >> tee -a instFromSource.err ; fi        
instFromSource http://johnvansickle.com/ffmpeg/git-source/libwebp-0.4.4.tar.xz libwebp            ; if [ $? -gt 0 ] ; then echo FEHLER:libwebp      >> tee -a instFromSource.err ; else echo OK:libwebp      >> tee -a instFromSource.err ; fi        
instFromSource http://johnvansickle.com/ffmpeg/git-source/opencore-amr-0.1.3.tar.xz  opencore-amr ; if [ $? -gt 0 ] ; then echo FEHLER:opencore-amr >> tee -a instFromSource.err ; else echo OK:opencore-amr >> tee -a instFromSource.err ; fi  
instFromSource http://johnvansickle.com/ffmpeg/git-source/opus-1.1.1.tar.xz opus                  ; if [ $? -gt 0 ] ; then echo FEHLER:opus         >> tee -a instFromSource.err ; else echo OK:opus         >> tee -a instFromSource.err ; fi            
instFromSource http://johnvansickle.com/ffmpeg/git-source/speex-1.2rc2.tar.xz speex               ; if [ $? -gt 0 ] ; then echo FEHLER:speex        >> tee -a instFromSource.err ; else echo OK:speex        >> tee -a instFromSource.err ; fi              
instFromSource http://johnvansickle.com/ffmpeg/git-source/vo-amrwbenc-0.1.3.tar.xz vo-amrwbenc    ; if [ $? -gt 0 ] ; then echo FEHLER:vo-amrwbenc  >> tee -a instFromSource.err ; else echo OK:vo-amrwbenc  >> tee -a instFromSource.err ; fi   



highlight<<yello
## Build ffmpeg 

## frei0r-plugins-1.4 
yello
cd 
wget http://johnvansickle.com/ffmpeg/git-source/frei0r-plugins-1.4.tar.xz
tar xzf frei0r-plugins-1.4.tar.xz
cd frei0r-plugins-1.4
./configure
make 
checkinstall


highlight<<yello
## x265 
yello

cd 
hg clone https://albus-fh@bitbucket.org/multicoreware/x265
cd x265/build/linux
./configure
./make-Makefiles.bash
make 
checkinstall --pkgname=x265 --backup=no --deldoc=yes \
		--fstrans=no --default 

highlight<<yello
## x264
## H.264 [x264 Encoding Guide](https://ffmpeg.org/trac/ffmpeg/wiki/x264EncodingGuide)
yello

cd
git clone --depth 1 git://git.videolan.org/x264
cd x264
./configure --enable-static
make
checkinstall --pkgname=x264 --pkgversion="3:$(./version.sh | \
	  awk -F'[" ]' '/POINT/{print $4"+git"$5}')" --backup=no --deldoc=yes \
	    --fstrans=no --default
highlight<<yello
## libfdk-aac
## AAC audio encoder.
yello

cd
wget http://downloads.sourceforge.net/opencore-amr/fdk-aac-0.1.0.tar.gz
tar xzvf fdk-aac-0.1.0.tar.gz
cd fdk-aac-0.1.0
./configure
make
checkinstall --pkgname=fdk-aac --pkgversion="0.1.0" --backup=no \
	  --deldoc=yes --fstrans=no --default
highlight<<yello
## libvpx
## VP8 video encoder and decoder.
yello
cd
git clone --depth 1 https://github.com/webmproject/libvpx.git
cd libvpx
./configure
make
checkinstall --pkgname=libvpx --pkgversion="1:$(date +%Y%m%d%H%M)-git" --backup=no \
	  --deldoc=yes --fstrans=no --default

highlight<<yello
## libmfx
yello
cd 
git clone https://github.com/lu-zero/mfx_dispatch.git	
cd mfx_dispatch
autoreconf -i
./configure --prefix=/usr
make -j 18
make install
checkinstall --pkgname=libmfx --backup=no \
	  --deldoc=yes --fstrans=no --default

	  
highlight<<yello
## libsoxr
yello
cd
git clone git://git.code.sf.net/p/soxr/code soxr-code  
cd soxr-code
cmake .
checkinstall --pkgname=libsoxr --pkgversion="1:$(date +%Y%m%d%H%M)-git" --backup=no \
	  --deldoc=yes --fstrans=no --default


	  
highlight<<yello
## FFmpeg
yello
cd
git clone --depth 1 git://source.ffmpeg.org/ffmpeg
cd ffmpeg
./configure --enable-gpl \
--enable-libfaac  \
--enable-nonfree \
--enable-version3 \
--enable-x11grab \
    --disable-shared \
    --disable-debug \
    --enable-runtime-cpudetect \
    --enable-libmp3lame \
    --enable-libx264 \
    --enable-libx265 \
    --enable-libwebp \
    --enable-libvorbis \
    --enable-libvpx \
    --enable-libfreetype \
    --enable-fontconfig \
    --enable-libxvid \
    --enable-libopencore-amrnb \
    --enable-libopencore-amrwb \
    --enable-libtheora \
    --enable-gray \
    --enable-libopenjpeg \
    --enable-libopus \
    --enable-libass \
    --enable-gnutls \
    --enable-libsoxr \
    --enable-frei0r \
    --enable-libfribidi \
    --disable-indev=sndio \
    --disable-outdev=sndio \
    --enable-librtmp \
    --enable-libmfx \
    --cc=gcc


make
checkinstall --pkgname=ffmpeg --pkgversion="5:$(date +%Y%m%d%H%M)-git" --backup=no \
	  --deldoc=yes --fstrans=no --default
hash x264 ffmpeg ffplay ffprobe

highlight<<yello
## qt-faststart
yello
cd ~/ffmpeg
make tools/qt-faststart
checkinstall --pkgname=qt-faststart --pkgversion="$(date +%Y%m%d%H%M)-git" --backup=no \
	  --deldoc=yes --fstrans=no --default install -Dm755 tools/qt-faststart \
	    /usr/local/bin/qt-faststart
highlight<<yello
## lavf support to x264
yello
cd ~/x264
make distclean
./configure --enable-static
make
checkinstall --pkgname=x264 --pkgversion="3:$(./version.sh | \
	  awk -F'[" ]' '/POINT/{print $4"+git"$5}')" --backup=no --deldoc=yes \
	    --fstrans=no --default


highlight<<yello
## delete unnecessary files	
yello
rm -Rf ~/fdk-aac-0.1.0
rm -Rf ~/fdk-aac-0.1.0.tar.gz
rm -Rf ~/ffmpeg
rm -Rf ~/libvpx-1.5.0
rm -Rf ~/libvpx-1.5.0.tar.bz2
rm -Rf ~/x264
rm -Rf ~/x265
rm -Rf ~/ffmpeg
rm -Rf ~/freetype-2.6.1
rm -Rf ~/freetype-2.6.1.tar.xz
rm -Rf ~/frei0r-plugins-1.4
rm -Rf ~/frei0r-plugins-1.4.tar.xz
rm -Rf ~/lame-3.99.5
rm -Rf ~/lame-3.99.5.tar.xz
rm -Rf ~/libass-git.tar.xz
rm -Rf ~/libsoxr-git.tar.xz
rm -Rf ~/libvidstab-git.tar.xz
rm -Rf ~/libvorbis-1.3.4.tar.xz
rm -Rf ~/libvpx-git.tar.xz
rm -Rf ~/libwebp-0.4.4.tar.xz
rm -Rf ~/opencore-amr-0.1.3.tar.xz
rm -Rf ~/opus-1.1.1.tar.xz
rm -Rf ~/vo-amrwbenc-0.1.3.tar.xz
rm -Rf ~/vo-amrwbenc*
rm -Rf ~/soxr-code

